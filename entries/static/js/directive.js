app.directive("orderDetails", function() {
  return {
    templateUrl: '/static/ngTemplates/directive/orderDetails.html',
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
    },
    controller: ["$scope", "$element", "$http",
      function($scope, $element, $http) {
        $scope.toCollect=false
        $scope.activeBtn='z_btn z_primary'
          $scope.data={
            toDeliverd:[
              {
                name:'Power of your subconcious',
                author:'Joseph Murphy',
                img:"/static/imgs/power.jpg",
              },
              {
                name:'Think and grow rich',
                author:'Daniel sandow',
                img:"/static/imgs/think.jpg"
              },
              {
                name:'Substances',
                author:'Allen',
                img:"/static/imgs/substances.jpg"
              },
              {
                name:'Catalyst',
                author:'Ramanadan',
                img:"/static/imgs/catalyst.jpg"
              }
            ],
            toCollect:[
              {
                name:'Zero to One',
                author:'Martin author',
                img:"/static/imgs/zero.jpg"
              },
              {
                name:'Private India',
                author:'Ashwin Sanghi',
                img:"/static/imgs/private.jpg"
              },
              {
                name:'World Tour',
                author:'Praveen Tyagi',
                img:"/static/imgs/world.jpg"
              },
            ],
            orderedOn: '11/7/2018',
            orderType: 'College Club',
            userName: 'Nandha Kumar',
            phoneNumber: 1234567,
            email: 'Nandha@zypher.co',
            address: 'Bangalore',
        }



        $scope.setType=function(input){
          if(input=='deleiver'){
            $scope.toCollect=false
          }else{
            $scope.toCollect=true
          }
        }


      }
    ]
  };
});




app.directive("userDetails", function() {
  return {
    templateUrl: '/static/ngTemplates/directive/userDetails.html',
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
    },
    controller: ["$scope", "$element", "$http",
      function($scope, $element, $http) {
      }
    ]
  };
});









app.directive("orderEdit", function() {
  return {
    templateUrl: '/static/ngTemplates/directive/orderEdit.html',
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
    },
    controller: ["$scope", "$element", "$http",
      function($scope, $element, $http) {
        console.log($scope.data);
      }
    ]
  };
});


app.directive("listUsers", function() {
  return {
    templateUrl: '/static/ngTemplates/directive/listUsers.html',
    restrict: 'E',
    replace: true,
    scope: {
      data: '=',
      title:'=',
    },
    controller: ["$scope", "$element", "$http",
      function($scope, $element, $http) {
        console.log($scope.data);
        $scope.users=$scope.data


        $scope.tableAction = function(action,target, mode) {
          console.log(target, action, mode);
          // console.log($scope.data.tableData);

              if (action == 'edit') {
                var title = 'Edit Contact :'+target;
                var appType = 'editOrder';
              } else if (action == 'details') {
                var title = 'Details :'+target;
                var appType = 'exploreOrder';
              }
              $scope.addTab({
                title: title,
                cancel: true,
                app: appType,
                data: {
                  pk: target,
                  // index: i
                },
                active: true
              })
            }





        // console.log($state.current.name);
        $scope.tabs = [];
        $scope.searchTabActive = true;

        $scope.closeTab = function(event,index) {
          event.preventDefault();
          $scope.tabs.splice(index, 1)
        }

        $scope.addTab = function(input) {
          console.log(JSON.stringify(input));
          $scope.searchTabActive = false;
          let alreadyOpen = false;
          for (var i = 0; i < $scope.tabs.length; i++) {
            $scope.tabs[i].active = true;
            if ($scope.tabs[i].data.pk == input.data.pk && $scope.tabs[i].app == input.app) {
              $scope.tabs[i].active = true;
              alreadyOpen = true;
            } else {
              $scope.tabs[i].active = false;
            }
          }
          if (!alreadyOpen) {
            $scope.tabs.push(input)
          }
        }
      }
    ]
  };
});
