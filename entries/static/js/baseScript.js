var app = angular.module('app', ['ui.router','ui.bootstrap']);

app.config(function($stateProvider, $urlRouterProvider,$httpProvider) {



  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  $httpProvider.defaults.withCredentials = true;

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/static/ngTemplates/entries.dashboard.html',
            controller:'entries.dashboard'
        })
        .state('orders', {
            url: '/orders',
            templateUrl: '/static/ngTemplates/entries.orders.html',
            controller:'entries.orders'
        })
        .state('users', {
            url: '/users',
            templateUrl: '/static/ngTemplates/entries.users.html',
            controller:'entries.users'
        })
        .state('clubs', {
            url: '/clubs',
            templateUrl: '/static/ngTemplates/entries.clubs.html',
            controller:'entries.clubs'
        })
        .state('events', {
            url: '/events',
            templateUrl: '/static/ngTemplates/entries.events.html',
            controller:'entries.events'
        })
        .state('inventory', {
            url: '/inventory',
            templateUrl: '/static/ngTemplates/entries.inventory.html',
            controller:'entries.inventory'
        })




});

app.controller('main',['$scope','$http',function($scope,$http){

  $scope.sideMenu = '/static/ngTemplates/entries.sideMenu.html'
  $scope.header = '/static/ngTemplates/entries.header.html'
  $scope.active=[true,false,false,false,false,false]
  $scope.titels=['Dashboard','Order Management','User Management','Club Management','Event Management','Inventory']
  $scope.title=$scope.titels[0]
  $scope.setActive=function(input){
    for (var i = 0; i < $scope.active.length; i++) {
      $scope.active[i]=false
    }
    $scope.active[input]=true;
    $scope.title=$scope.titels[input]
  }

  $scope.sideBarActive=false;
  $scope.sidebarCollapse=function(){
    $scope.sideBarActive=!$scope.sideBarActive;
  }

}])




app.controller('entries.dashboard',['$scope','$http','$timeout',function($scope,$http,$timeout){
  $scope.name="dashboard"

}])


app.controller('entries.orders',['$scope','$http','$timeout','$state',function($scope,$http,$timeout,$state){
  $scope.name="orders"
  $scope.myTemplate='/static/ngTemplates/templates/orders/entries.orders.order.html'
  $scope.selectedTemp='personal'
  $scope.setTemplate=function(input){
    if(input=='personal'){
      $scope.selectedTemp='personal'
        $scope.myTemplate='/static/ngTemplates/templates/orders/entries.orders.order.html'
    }else if(input=='club'){
      $scope.selectedTemp='club'
        $scope.myTemplate='/static/ngTemplates/templates/orders/entries.orders.club.html'
    }else{
      $scope.selectedTemp='procure'
        $scope.myTemplate='/static/ngTemplates/templates/orders/entries.orders.procure.html'
    }
  }

  $scope.orders=[{
    name:'Abc',
    count:3,
    id:0,
    Received_at:'10:30'
  },{
    name:'xyz',
    count:2,
    id:1,
    Received_at:'11:30'
  },{
    name:'cgh',
    count:1,
    id:2,
    Received_at:'10:00'
  },{
    name:'cgdfdh',
    count:1,
    id:5,
    Received_at:'10:00'
  },{
    name:'cgffh',
    count:1,
    id:4,
    Received_at:'14:00'
  },{
    name:'some',
    count:8,
    id:3,
    Received_at:'08:30'
  }]


  $scope.totalItems = 64;
  $scope.currentPage = 4;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;




  $scope.tableAction = function(action,target, mode) {
    console.log(target, action, mode);
    // console.log($scope.data.tableData);

        if (action == 'edit') {
          var title = 'Edit Contact :'+target;
          var appType = 'editOrder';
        } else if (action == 'details') {
          var title = 'Details :'+target;
          var appType = 'exploreOrder';
        }
        $scope.addTab({
          title: title,
          cancel: true,
          app: appType,
          data: {
            pk: target,
            // index: i
          },
          active: true
        })
      }





  console.log($state.current.name);
  $scope.tabs = [];
  $scope.searchTabActive = true;

  $scope.closeTab = function(event,index) {
    event.preventDefault();
    $scope.tabs.splice(index, 1)
  }

  $scope.addTab = function(input) {
    console.log(JSON.stringify(input));
    $scope.searchTabActive = false;
    let alreadyOpen = false;
    for (var i = 0; i < $scope.tabs.length; i++) {
      $scope.tabs[i].active = true;
      if ($scope.tabs[i].data.pk == input.data.pk && $scope.tabs[i].app == input.app) {
        $scope.tabs[i].active = true;
        alreadyOpen = true;
      } else {
        $scope.tabs[i].active = false;
      }
    }
    if (!alreadyOpen) {
      $scope.tabs.push(input)
    }
  }

}])



app.controller('entries.users',['$scope','$http','$timeout',function($scope,$http,$timeout){
  $scope.name="users"
  $scope.myTemplate='/static/ngTemplates/templates/users/entries.users.all.html'
  $scope.selectedTemp='all'

  $scope.setTemplate=function(input){
    if(input=='all'){
      $scope.selectedTemp='all'
        $scope.myTemplate='/static/ngTemplates/templates/users/entries.users.all.html'
    }else if(input=='club'){
      $scope.selectedTemp='club'
        $scope.myTemplate='/static/ngTemplates/templates/users/entries.users.club.html'
    }else{
      $scope.selectedTemp='subscribed'
        $scope.myTemplate='/static/ngTemplates/templates/users/entries.orders.subscribed.html'
    }
  }

  $scope.users=[{
    name:'John',
    phoneNumber:36454545,
    email:'John@Zypher.co',
  },{
    name:'Harry',
    phoneNumber:364545545,
    email:'Harry@Zypher.co',
  },{
    name:'Danial',
    phoneNumber:123456,
    email:'Danial@Zypher.co',
  },{
    name:'Max',
    phoneNumber:989895,
    email:'Max@Zypher.co',
  },{
    name:'Balram',
    phoneNumber:36454545,
    email:'Balram@Zypher.co',
  },{
    name:'Nandha',
    phoneNumber:36454545,
    email:'Nandha@Zypher.co',
  }]


  $scope.totalItems = 64;
  $scope.currentPage = 4;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;




  // $scope.tableAction = function(action,target, mode) {
  //   console.log(target, action, mode);
  //   // console.log($scope.data.tableData);
  //
  //       if (action == 'edit') {
  //         var title = 'Edit Contact :'+target;
  //         var appType = 'editOrder';
  //       } else if (action == 'details') {
  //         var title = 'Details :'+target;
  //         var appType = 'exploreOrder';
  //       }
  //       $scope.addTab({
  //         title: title,
  //         cancel: true,
  //         app: appType,
  //         data: {
  //           pk: target,
  //           // index: i
  //         },
  //         active: true
  //       })
  //     }
  //
  //
  //
  //
  //
  // // console.log($state.current.name);
  // $scope.tabs = [];
  // $scope.searchTabActive = true;
  //
  // $scope.closeTab = function(event,index) {
  //   event.preventDefault();
  //   $scope.tabs.splice(index, 1)
  // }
  //
  // $scope.addTab = function(input) {
  //   console.log(JSON.stringify(input));
  //   $scope.searchTabActive = false;
  //   let alreadyOpen = false;
  //   for (var i = 0; i < $scope.tabs.length; i++) {
  //     $scope.tabs[i].active = true;
  //     if ($scope.tabs[i].data.pk == input.data.pk && $scope.tabs[i].app == input.app) {
  //       $scope.tabs[i].active = true;
  //       alreadyOpen = true;
  //     } else {
  //       $scope.tabs[i].active = false;
  //     }
  //   }
  //   if (!alreadyOpen) {
  //     $scope.tabs.push(input)
  //   }
  // }

}])










app.controller('entries.clubs',['$scope','$http','$timeout','$uibModal',function($scope,$http,$timeout,$uibModal){
  $scope.name="clubs"
  $scope.clubs=[]

  $scope.addClub = function(){
    $uibModal.open({
      templateUrl: '/static/ngTemplates/uibModal/entries.club.newClub.modal.html',
      size: 'xl',
      backdrop: true,
      resolve: {
        data: function() {
          // return $scope.myChatThreadData;
          return $scope.clubs
        }
      },
      controller: function($scope, data , $uibModalInstance) {

        // $scope.myChatThreadData = myChatThreadData
        $scope.form={
          name:'',
          pocMail:'',
          pocName:'',
          url:'',
          passkey:''
        }

        $scope.submitFormData=function(){
          console.log($scope.form);
          data.push($scope.form)
          $uibModalInstance.dismiss();
        }
        // let status=false


      },
    }).result.then(function () {

    }, function () {

      // if (status != 'backdrop click' && status != 'escape key press') {
      //   // $scope.myChatThreadData.status = status
      // }
    });

  }

}])















app.controller('entries.events',['$scope','$http','$timeout',function($scope,$http,$timeout){
  $scope.name="events"

}])

app.controller('entries.inventory',['$scope','$http','$timeout',function($scope,$http,$timeout){
  $scope.name="inventory"

}])
